
const isInteger = value => Number.isInteger(parseFloat(value));


const getInputInteger = message => {
  let input = prompt(message);
  while (!isInteger(input)) {
    input = prompt("Please enter a valid integer: ");
  }
  return parseInt(input);
};


const userInput = getInputInteger("Enter a number: ");


const divisibleByFive = [];
for (let i = 0; i <= userInput; i++) {
  if (i % 5 === 0) {
    divisibleByFive.push(i);
  }
}


if (divisibleByFive.length === 0) {
  console.log("Sorry, no numbers.");
} else {
  console.log(`Numbers divisible by 5 from 0 to ${userInput}:`, divisibleByFive);
}


const isPrime = number => {
  if (number <= 1) {
    return false;
  }
  for (let i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      return false;
    }
  }
  return true;
};


const m = getInputInteger("Enter the first number (m): ");
const n = getInputInteger("Enter the second number (n): ");


if (m > n) {
  console.log("Error: m should be less than or equal to n.");
} else {
  const primeNumbers = [];
  for (let i = m; i <= n; i++) {
    if (isPrime(i)) {
      primeNumbers.push(i);
    }
  }

  if (primeNumbers.length === 0) {
    console.log("Sorry, no prime numbers in the given range.");
  } else {
    console.log(`Prime numbers in the range from ${m} to ${n}:`, primeNumbers);
  }
}
